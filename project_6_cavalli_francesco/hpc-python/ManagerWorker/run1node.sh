#!/bin/bash

splits=(50 100)
num_ranks=(2 4 6 8 9 12 16 20 24 28 32)
# grid_sizes=(128 256)
# num_ranks=(2 4 6)

mkdir 1node_output

for split in ${splits[@]}
do
    for ranks in ${num_ranks[@]}
    do
        sbatch --ntasks ${ranks} --nodes 1 --wrap="mpirun python3 manager_worker.py 4001 4001 ${split}" --output="1node_output/${ranks}_${split}.out" 
    done
done