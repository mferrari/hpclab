import re
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


ntasks=[50,100]
num_ranks=[2,4,6,8,9,12,16,20,24,28,32]

def extract_seconds(file_path):
    seconds = None
    with open(file_path, 'r') as file:
        content = file.read()
        match = re.search(r'Run took (\d+(\.\d+)?) seconds', content)
        if match:
            seconds = float(match.group(1))
    return seconds

data = {"n": [], "sec_1node_50": [], "sec_1th_50": [],  "sec_1node_100": [], "sec_1th_100": []}

for nranks in num_ranks:
    file_path_1node_50 = f"1node_output/{nranks}_{ntasks[0]}.out"
    file_path_1th_50 = f"1th_output/{nranks}_{ntasks[0]}.out"
    file_path_1node_100 = f"1node_output/{nranks}_{ntasks[1]}.out"
    file_path_1th_100 = f"1th_output/{nranks}_{ntasks[1]}.out"
    iters_per_second_1node_50 = extract_seconds(file_path_1node_50)
    iters_per_second_1th_50 = extract_seconds(file_path_1th_50)
    iters_per_second_1node_100 = extract_seconds(file_path_1node_100)
    iters_per_second_1th_100 = extract_seconds(file_path_1th_100)
    data["n"].append(nranks)
    data["sec_1node_50"].append(iters_per_second_1node_50)
    data["sec_1th_50"].append(iters_per_second_1th_50)
    data["sec_1node_100"].append(iters_per_second_1node_100)
    data["sec_1th_100"].append(iters_per_second_1th_100)

data_df = pd.DataFrame(data)

# strong scaling
ax = data_df.plot(x='n', y=['sec_1node_50', 'sec_1th_50','sec_1node_100', 'sec_1th_100'], marker='o', linestyle='-')
ax.set_xlabel('n process')
ax.set_ylabel('Seconds')
ax.set_title(f'Strong scaling with ntasks {ntasks}')
plt.legend(["1 node, multiple process, 50 tasks", "1 process per node, multiple nodes 50 tasks", "1 node, multiple process, 100 tasks", "1 process per node, multiple nodes 100 tasks"])
plt.savefig(f"figs/strong_{ntasks}.png")


# weak scaling
# sel_ntasks = grid_ntaskss
# sel_process = [2,4,8,16]
# xAxis = []
# yAxis_1node =[]
# yAxis_1th =[]
# for (ntasks, n) in zip(sel_ntasks, sel_process):
#     value = data_df[(data_df['ntasks'] == ntasks) & (data_df['n'] == n)]
#     yAxis_1node.append(value["sec_1node"].values[0])
#     yAxis_1th.append(value["sec_1th"].values[0])
#     xAxis.append(n)

# plt.clf()
# plt.plot(np.log2(xAxis), yAxis_1node, marker='o', linestyle='-')
# plt.plot(np.log2(xAxis), yAxis_1th, marker='o', linestyle='-')
# plt.xlabel('(ntasks, n process)')
# plt.ylabel('Seconds')
# plt.title(f'Weak scaling')
# plt.legend(["1 node, multiple process", "1 process per node, multiple nodes"])
# plt.xticks(np.log2(xAxis), [f"(s={sel_ntasks[i]},n={sel_process[i]})" for i in range(0, len(sel_process))], fontntasks=8)
# plt.savefig(f"figs/weak.png")
