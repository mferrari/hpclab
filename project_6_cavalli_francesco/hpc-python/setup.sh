#!/bin/bash

module load module gcc openmpi python hdf5
python -m venv project6-env
source project6-env/bin/activate
pip install numpy scipy matplotlib mpi4py h5py pandas