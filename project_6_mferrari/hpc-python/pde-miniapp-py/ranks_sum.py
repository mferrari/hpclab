from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# define receive buffer for pickle method (reduce with sum with pickle method)
pickle_recv = comm.reduce(rank, op = MPI.SUM, root=0)

# create send and receive buffers, C-like version
send = np.array([rank])
recv = np.zeros_like(send)

# reduce C-like version
comm.Reduce(send, recv, op=MPI.SUM, root=0)

if rank == 0:
    sum = np.zeros((1,), 'i')
    for i in range(size):
        sum = sum + i
    print(f"Sum of ranks from 0 to {size-1} should yield {int(sum)}.")
    print(f"Pickle-method yields {pickle_recv}.")
    print(f"Fast method yields {int(recv)}.")

