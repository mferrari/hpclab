import re
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


grid_sizes=[128,256,512,1024]
num_ranks=[1,2,4,6,8,9,12,16,20,24,28,32]

def extract_iters_per_second(file_path):
    iters_per_second = 0
    with open(file_path, 'r') as file:
        content = file.read()
        match = re.search(r'(\d+(\.\d+)?)\s*iters/second', content)
        if match:
            iters_per_second = float(match.group(1))
    return iters_per_second

data = {"size": [], "n": [], "iter_sec_1node": [], "iter_sec_1th": []}

for size in grid_sizes:
    for nranks in num_ranks:
        file_path_1node = f"1node_output/{nranks}_{size}.out"
        file_path_1th = f"1th_output/{nranks}_{size}.out"
        iters_per_second_1node = extract_iters_per_second(file_path_1node)
        iters_per_second_1th = extract_iters_per_second(file_path_1th)
        data["size"].append(size)
        data["n"].append(nranks)
        data["iter_sec_1node"].append(iters_per_second_1node)
        data["iter_sec_1th"].append(iters_per_second_1th)

data_df = pd.DataFrame(data)

# strong scaling
for size in grid_sizes:
    filtered_df = data_df[data_df['size'] == size]
    ax = filtered_df.plot(x='n', y=['iter_sec_1node', 'iter_sec_1th'], marker='o', linestyle='-')
    ax.set_xlabel('n process')
    ax.set_ylabel('Iterations per Second')
    ax.set_title(f'Strong scaling with size {size}')
    plt.legend(["1 node, multiple process", "1 process per node, multiple nodes"])
    plt.savefig(f"strong_{size}.png", dpi = 300)


# weak scaling
sel_size = grid_sizes
sel_process = [2,4,8,16]
xAxis = []
yAxis_1node =[]
yAxis_1th =[]
for (size, n) in zip(sel_size, sel_process):
    value = data_df[(data_df['size'] == size) & (data_df['n'] == n)]
    yAxis_1node.append(value["iter_sec_1node"].values[0])
    yAxis_1th.append(value["iter_sec_1th"].values[0])
    xAxis.append(n)

plt.clf()
plt.plot(np.log2(xAxis), yAxis_1node, marker='o', linestyle='-')
plt.plot(np.log2(xAxis), yAxis_1th, marker='o', linestyle='-')
plt.xlabel('(size, n process)')
plt.ylabel('Iterations per Second')
plt.title(f'Weak scaling')
plt.legend(["1 node, multiple process", "1 process per node, multiple nodes"])
plt.xticks(np.log2(xAxis), [f"(s={sel_size[i]},n={sel_process[i]})" for i in range(0, len(sel_process))], fontsize=8)
plt.savefig(f"weak.png", dpi=300)