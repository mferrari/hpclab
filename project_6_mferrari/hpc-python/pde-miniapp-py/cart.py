# THIS CODE IS SOLUTION TO TWO SUBTASKS: 2.2 and 2.3


from mpi4py import MPI
import numpy as np

# define communicator, size and rank
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

dims = MPI.Compute_dims(size, 2)

#initialize the communicator
cart_comm = comm.Create_cart(dims, periods=[True,True], reorder = False)
cart_rank = cart_comm.Get_rank()
cart_coords = cart_comm.Get_coords(rank)

#get neighbours
west, east = cart_comm.Shift(direction = 1, disp = 1)
south, north = cart_comm.Shift(direction = 0, disp = 1)

# Output subexercise 2.3
print(f'Current rank: {rank}. S: {south}, N: {north}, W: {west}, E: {east}, coordinates: {cart_coords}.')

req1 = cart_comm.irecv(source=south, tag=11)
req2 = cart_comm.isend(rank, dest=north, tag=11)

req3 = cart_comm.irecv(source=north, tag=11)
req4 = cart_comm.isend(rank, dest=south, tag=11)

req5 = cart_comm.irecv(source=east, tag=11)
req6 = cart_comm.isend(rank, dest=west, tag=11)

req7 = cart_comm.irecv(source=west, tag=11)
req8 = cart_comm.isend(rank, dest=east, tag=11)

rank_s = req1.wait()
req2.wait()

rank_n = req3.wait()
req4.wait()

rank_e = req5.wait()
req6.wait()

rank_w = req7.wait()
req8.wait()

# Output subexercise 2.4:

if south != rank_s:
    print("South: incorrect")

if north != rank_n:
    print("North: incorrect")

if east != rank_e:
    print("East: incorrect")

if west != rank_w:
    print("West: incorrect")

if(west == rank_w and south == rank_s and north == rank_n and east == rank_e):
    print("Everything correct :-)")
