#!/bin/bash

#SBATCH -n 4
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=1
#SBATCH --time=4:00:00
#SBATCH --job-name="pde-miniapp-py"

time -p mpirun python manager_worker.py 4001 4001 100 2>&1 | tee my.log
exit
