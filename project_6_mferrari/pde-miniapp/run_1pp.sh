#!/bin/bash

grid_sizes=(128 256 512 1024)
num_ranks=(1 2 4 6 8 9 12 16 20 24 28 32)
# grid_sizes=(128 256)
# num_ranks=(2 4 6)


mkdir 1th_output

for grid_size in ${grid_sizes[@]}
do
    for ranks in ${num_ranks[@]}
    do
        sbatch --ntasks ${ranks} --nodes ${ranks} --wrap="mpirun ./main ${grid_size} 100 0.005" --output="1th_output/${ranks}_${grid_size}.out" 
    done
done