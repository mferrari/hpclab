#   M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
#   Data extraction method
"""
    read_csv_graph(path_file)

Extract the data located in the csv `path_file`.
"""
function read_csv_graph(path_file)
    # Steps
    #   1.  Load the .csv files
    #       see readdlm(...)

    #   2.  Construct the adjacency matrix A

    #   3.  Visualize and save the result 
    #       use drawGraph(A, coords)

    #   4.  Return the matrix A and the coordinates 
    #       return(A, coords)
end