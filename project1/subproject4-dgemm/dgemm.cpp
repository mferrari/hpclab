#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <limits>
#include <assert.h>
#include <iostream>
#include <immintrin.h>  // AVX
#include "Stopwatch.h"

void dgemm(double* A, double* B, double* C) {
    for (int n = 0; n < N; ++n) {
    for (int k = 0; k < K; ++k) {
      for (int m = 0; m < M; ++m) {
        C[m + n*M] += A[m + k*M] * B[k + n*K];
      }
    }
  }
}

void dgemm_opt(double* __restrict A, double* __restrict B, double* __restrict C) {
  
  __m256d c0, c1, c2, c3, c4, c5, c6, c7;
  __m256d a0, a1, a2, a3, a4, a5, a6, a7;

  for (int n = 0; n < N; ++n) {
    c0 = _mm256_setzero_pd();
    c1 = _mm256_setzero_pd();
    c2 = _mm256_setzero_pd();
    c3 = _mm256_setzero_pd();
    c4 = _mm256_setzero_pd();
    c5 = _mm256_setzero_pd();
    c6 = _mm256_setzero_pd();
    c7 = _mm256_setzero_pd();

    for (int k = 0; k < K; ++k) {

      __m256d b = _mm256_broadcast_sd(B + k + n*K);

      // Load all a vectors
      // Hardware prefetching does heavy lifting
      a0 = _mm256_load_pd(A + k*M);
      a1 = _mm256_load_pd(A + 4 + k*M);
      a2 = _mm256_load_pd(A + 8 + k*M);
      a3 = _mm256_load_pd(A + 12 + k*M);
      a4 = _mm256_load_pd(A + 16 + k*M);
      a5 = _mm256_load_pd(A + 20 + k*M);
      a6 = _mm256_load_pd(A + 24 + k*M);
      a7 = _mm256_load_pd(A + 28 + k*M);

      // Strong hint to register accumulation      
      c0 = _mm256_fmadd_pd(a0, b, c0);
      c1 = _mm256_fmadd_pd(a1, b, c1);
      c2 = _mm256_fmadd_pd(a2, b, c2);
      c3 = _mm256_fmadd_pd(a3, b, c3);
      c4 = _mm256_fmadd_pd(a4, b, c4);
      c5 = _mm256_fmadd_pd(a5, b, c5);
      c6 = _mm256_fmadd_pd(a6, b, c6);
      c7 = _mm256_fmadd_pd(a7, b, c7);


    }
    // Store once per 128 iterations of k
    _mm256_store_pd(C + n*M, c0);
    _mm256_store_pd(C + 4 + n*M, c1);
    _mm256_store_pd(C + 8 + n*M, c2);
    _mm256_store_pd(C + 12 + n*M, c3);
    _mm256_store_pd(C + 16 + n*M, c4);
    _mm256_store_pd(C + 20 + n*M, c5);
    _mm256_store_pd(C + 24 + n*M, c6);
    _mm256_store_pd(C + 28 + n*M, c7);

  }
}



int main(int argc, char** argv) {
  int repetitions = 10000;
  if (argc > 1) {
    repetitions = atoi(argv[1]);
  }
  
  /** Allocate memory */
  double* A, *B, *C, *A_test, *B_test, *C_test;
  
  posix_memalign(reinterpret_cast<void**>(&A),      ALIGNMENT, M*K*sizeof(double));
  posix_memalign(reinterpret_cast<void**>(&B),      ALIGNMENT, K*N*sizeof(double));
  posix_memalign(reinterpret_cast<void**>(&C),      ALIGNMENT, M*N*sizeof(double));
  posix_memalign(reinterpret_cast<void**>(&A_test), ALIGNMENT, M*K*sizeof(double));
  posix_memalign(reinterpret_cast<void**>(&B_test), ALIGNMENT, K*N*sizeof(double));
  posix_memalign(reinterpret_cast<void**>(&C_test), ALIGNMENT, M*N*sizeof(double));

  for (int j = 0; j < K; ++j) {
    for (int i = 0; i < M; ++i) {
      A[j*M + i] = i + j;
    }
  }
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < K; ++i) {
      B[j*K + i] = (K-i) + (N-j);
    }
  }
  memset(C, 0, M*N*sizeof(double));
  memcpy(A_test, A, M*K*sizeof(double));
  memcpy(B_test, B, K*N*sizeof(double));
  memset(C_test, 0, M*N*sizeof(double));
  
  /** Check correctness of optimised dgemm */
  #pragma noinline
  {
    dgemm(A, B, C);
    dgemm_opt(A_test, B_test, C_test);
  }
  

  double error = 0.0;
  for (int i = 0; i < M*N; ++i) {
    double diff = C[i] - C_test[i];
    error += diff*diff;
  }
  error = sqrt(error);
  if (error > std::numeric_limits<double>::epsilon()) {
    printf("Optimised DGEMM is incorrect. Error: %e\n", error);
    return -1;
  }

  /** Test performance of optimised dgemm */
  
  #pragma noinline
  dgemm_opt(A, B, C);
  
  Stopwatch stopwatch;
  stopwatch.start();

  #pragma noinline
  for (int r = 0; r < repetitions; ++r) {
    dgemm_opt(A, B, C);
  }
  
  double time = stopwatch.stop();
  printf("%lf ms, %lf GFLOP/s\n", time * 1.0e3, repetitions*2.0*M*N*K/time * 1.0e-9);
  
  /** Clean up */
  
  free(A); free(B); free(C);
  free(A_test); free(B_test); free(C_test);

  return 0;
}
