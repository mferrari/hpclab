
N = 32
M = 32
K = 128



# C
for n in range(32*8):
    s = f"__m256d c{n} = _mm256_setzero_pd();"
    print(s)

print()
# load all a values
for n in range(128* 8):
    s = f"__m256d a{n} = _mm256_load_pd(A + {4*n});"
    print(s)

# fma
for n in range(32):
    for m in range(8):
        s = f"c{n}_{m} = _mm256_load_pd(C + {m*4 + n*32});"
        print(s)


# for k in range(128):
#     s = f"__m256d b{k} = _mm256_broadcast_sd(B + {k});"
#     print(s)