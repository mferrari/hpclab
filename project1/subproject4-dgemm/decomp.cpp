#include <immintrin.h>  // AVX

void dgemm_opt(double* A, double* B, double* C) {
  
  __m256d B_v, A_v, rax;// r1, r2, r3, r4, rax;

  // Transpose A
  double AT[K*M];

  for(int k=0; k < K; ++k){
    for(int m=0; m<M; ++m){
      AT[m*K + k] = A[m + k*M];
    }
  }

  for (int n = 0; n < N; ++n) {
    for (int m = 0; m < M; ++m) {
      
      double tmp = 0.;

      for (int k = 0; k < K; k+=4) {
        B_v = _mm256_load_pd(B + n*K + k);
        A_v = _mm256_load_pd(AT + m*K + k);
        rax = _mm256_mul_pd(A_v, B_v);

        // double* pv = (double*)&rax;
        tmp += 1+ 2 + 3 + 4;
      }

      C[n*M + m] = tmp;
    }
  }
}