#include <immintrin.h>  // AVX
/* 
    Please include compiler name below (you may also include any other modules you would like to be loaded)

COMPILER= intel compilers 2022

env2lmod
module load intel/2022.1.2 // Not older versions!!

    Please include All compiler flags and libraries as you want them run. You can simply copy this over from the Makefile's first few lines
 
CC = icc
OPT = -O3 -funroll-loops -mavx2 -march=core-avx2 -fma -ftz -fomit-frame-pointer 
CFLAGS = -Wall -std=gnu99 $(OPT) -fopenmp
*/

const char* dgemm_desc = "Naive, three-loop dgemm.";

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */    
#define N 32

void square_dgemm (int s, double* A, double* B, double* C)
{
  // The easiest solution is to simply parallelize over the loops
  // of the indices of C (n, m). This prevents race conditions!
  #pragma omp parallel for collapse(2)
  for(int n = 0; n < s; n+= N){
    for(int m = 0; m < s; m += N){
      
      double * Cb = C + n*s + m;

      for(int k = 0; k < s; k += N){
        double * Bb = B + n*s + k;
        double * Ab = A + k*s + m;

        // Begin micro-kernel
        for (int nn = 0; nn < N; ++nn) {
            int nns = nn*s;
            __m256d c0 = _mm256_load_pd(Cb + nns);
            __m256d c1 = _mm256_load_pd(Cb + 4 + nns);
            __m256d c2 = _mm256_load_pd(Cb + 8 + nns);
            __m256d c3 = _mm256_load_pd(Cb + 12 + nns);
            __m256d c4 = _mm256_load_pd(Cb + 16 + nns);
            __m256d c5 = _mm256_load_pd(Cb + 20 + nns);
            __m256d c6 = _mm256_load_pd(Cb + 24 + nns);
            __m256d c7 = _mm256_load_pd(Cb + 28 + nns);
        
            for (int kk = 0; kk < N; ++kk) {
              int kks = kk*s;

              __m256d b = _mm256_broadcast_sd(Bb + kk + nns);

              // Load all a vectors
              // Hardware prefetching does heavy lifting
              __m256d a0 = _mm256_load_pd(Ab + kks);
              __m256d a1 = _mm256_load_pd(Ab + 4 + kks);
              __m256d a2 = _mm256_load_pd(Ab + 8 + kks);
              __m256d a3 = _mm256_load_pd(Ab + 12 + kks);
              __m256d a4 = _mm256_load_pd(Ab + 16 + kks);
              __m256d a5 = _mm256_load_pd(Ab + 20 + kks);
              __m256d a6 = _mm256_load_pd(Ab + 24 + kks);
              __m256d a7 = _mm256_load_pd(Ab + 28 + kks);

              // Strong hint to register accumulation      
              c0 = _mm256_fmadd_pd(a0, b, c0);
              c1 = _mm256_fmadd_pd(a1, b, c1);
              c2 = _mm256_fmadd_pd(a2, b, c2);
              c3 = _mm256_fmadd_pd(a3, b, c3);
              c4 = _mm256_fmadd_pd(a4, b, c4);
              c5 = _mm256_fmadd_pd(a5, b, c5);
              c6 = _mm256_fmadd_pd(a6, b, c6);
              c7 = _mm256_fmadd_pd(a7, b, c7);


            }
            
            // Store once per 128 iterations of kk
            _mm256_store_pd(Cb + nns, c0);
            _mm256_store_pd(Cb + 4 + nns, c1);
            _mm256_store_pd(Cb + 8 + nns, c2);
            _mm256_store_pd(Cb + 12 + nns, c3);
            _mm256_store_pd(Cb + 16 + nns, c4);
            _mm256_store_pd(Cb + 20 + nns, c5);
            _mm256_store_pd(Cb + 24 + nns, c6);
            _mm256_store_pd(Cb + 28 + nns, c7);

          }
        
        // End micro-kernel
      }
    }
  }

}

