#include <immintrin.h>  // AVX
#include <stdio.h>
#include <stdio.h>
/* 
    Please include compiler name below (you may also include any other modules you would like to be loaded)

COMPILER= gnu

    Please include All compiler flags and libraries as you want them run. You can simply copy this over from the Makefile's first few lines
 
CC = cc
OPT = -O3
CFLAGS = -Wall -std=gnu99 $(OPT)
MKLROOT = /opt/intel/composer_xe_2013.1.117/mkl
LDLIBS = -lrt -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm

*/

const char* dgemm_desc = "Naive, three-loop dgemm.";

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */    

#define N 32
#define M 32
#define K 32


void square_dgemm (int s, double* A, double* B, double* C)
{
  __m256d c0, c1, c2, c3, c4, c5, c6, c7;
  __m256d a0, a1, a2, a3, a4, a5, a6, a7;
  __m256d b;
  int nns, kks;
  double* Ab;
  double* Bb;
  double* Cb;


  for(int n = 0; n < s; n+= N){
    for(int k = 0; k < s; k += N){
      
      Bb = B + n*s + k;

      for(int m = 0; m < s; m += N){
        Ab = A + k*s + m;
        Cb = C + n*s + m;

        // Begin micro-kernel
        for (int nn = 0; nn < N; ++nn) {
            nns = nn*s;
            c0 = _mm256_load_pd(Cb + nns);
            c1 = _mm256_load_pd(Cb + 4 + nns);
            c2 = _mm256_load_pd(Cb + 8 + nns);
            c3 = _mm256_load_pd(Cb + 12 + nns);
            c4 = _mm256_load_pd(Cb + 16 + nns);
            c5 = _mm256_load_pd(Cb + 20 + nns);
            c6 = _mm256_load_pd(Cb + 24 + nns);
            c7 = _mm256_load_pd(Cb + 28 + nns);
        
            for (int kk = 0; kk < K; ++kk) {
              kks = kk*s;

              b = _mm256_broadcast_sd(Bb + kk + nns);

              // Load all a vectors
              // Hardware prefetching does heavy lifting
              a0 = _mm256_load_pd(Ab + kks);
              a1 = _mm256_load_pd(Ab + 4 + kks);
              a2 = _mm256_load_pd(Ab + 8 + kks);
              a3 = _mm256_load_pd(Ab + 12 + kks);
              a4 = _mm256_load_pd(Ab + 16 + kks);
              a5 = _mm256_load_pd(Ab + 20 + kks);
              a6 = _mm256_load_pd(Ab + 24 + kks);
              a7 = _mm256_load_pd(Ab + 28 + kks);

              // Strong hint to register accumulation      
              c0 = _mm256_fmadd_pd(a0, b, c0);
              c1 = _mm256_fmadd_pd(a1, b, c1);
              c2 = _mm256_fmadd_pd(a2, b, c2);
              c3 = _mm256_fmadd_pd(a3, b, c3);
              c4 = _mm256_fmadd_pd(a4, b, c4);
              c5 = _mm256_fmadd_pd(a5, b, c5);
              c6 = _mm256_fmadd_pd(a6, b, c6);
              c7 = _mm256_fmadd_pd(a7, b, c7);


            }
            
            // Store once per 128 iterations of kk
            _mm256_store_pd(Cb + nns, c0);
            _mm256_store_pd(Cb + 4 + nns, c1);
            _mm256_store_pd(Cb + 8 + nns, c2);
            _mm256_store_pd(Cb + 12 + nns, c3);
            _mm256_store_pd(Cb + 16 + nns, c4);
            _mm256_store_pd(Cb + 20 + nns, c5);
            _mm256_store_pd(Cb + 24 + nns, c6);
            _mm256_store_pd(Cb + 28 + nns, c7);

          }
        
        // End micro-kernel
      }
    }
  }

}
