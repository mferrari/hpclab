#include <stdio.h>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <limits>
#include <assert.h>
#include <iostream>
#include <stdlib.h>
#include <immintrin.h>  // AVX

void dgemm_opt(double* A, double* B, double* C) {
  
  __m256d c0, c1, c2, c3, c4, c5, c6, c7;
  __m256d a0, a1, a2, a3, a4, a5, a6, a7;

  for (int n = 0; n < N; ++n) {
    c0 = _mm256_load_pd(C + n*M);
    c1 = _mm256_load_pd(C + 4 + n*M);
    c2 = _mm256_load_pd(C + 8 + n*M);
    c3 = _mm256_load_pd(C + 12 + n*M);
    c4 = _mm256_load_pd(C + 16 + n*M);
    c5 = _mm256_load_pd(C + 20 + n*M);
    c6 = _mm256_load_pd(C + 24 + n*M);
    c7 = _mm256_load_pd(C + 28 + n*M);

    for (int k = 0; k < K; ++k) {

      __m256d b = _mm256_broadcast_sd(B + k + n*K);

      // Load all a vectors
      // Hardware prefetching does heavy lifting
      a0 = _mm256_load_pd(A + k*M);
      a1 = _mm256_load_pd(A + 4 + k*M);
      a2 = _mm256_load_pd(A + 8 + k*M);
      a3 = _mm256_load_pd(A + 12 + k*M);
      a4 = _mm256_load_pd(A + 16 + k*M);
      a5 = _mm256_load_pd(A + 20 + k*M);
      a6 = _mm256_load_pd(A + 24 + k*M);
      a7 = _mm256_load_pd(A + 28 + k*M);

      // Strong hint to register accumulation      
      c0 = _mm256_fmadd_pd(a0, b, c0);
      c1 = _mm256_fmadd_pd(a1, b, c1);
      c2 = _mm256_fmadd_pd(a2, b, c2);
      c3 = _mm256_fmadd_pd(a3, b, c3);
      c4 = _mm256_fmadd_pd(a4, b, c4);
      c5 = _mm256_fmadd_pd(a5, b, c5);
      c6 = _mm256_fmadd_pd(a6, b, c6);
      c7 = _mm256_fmadd_pd(a7, b, c7);


    }
    
    // Store once per 128 iterations of k
    _mm256_store_pd(C + n*M, c0);
    _mm256_store_pd(C + 4 + n*M, c1);
    _mm256_store_pd(C + 8 + n*M, c2);
    _mm256_store_pd(C + 12 + n*M, c3);
    _mm256_store_pd(C + 16 + n*M, c4);
    _mm256_store_pd(C + 20 + n*M, c5);
    _mm256_store_pd(C + 24 + n*M, c6);
    _mm256_store_pd(C + 28 + n*M, c7);

  }
}

int main() {
    // double A[32*32];
    // double B[32*32];    
    // double C[32*32];   

    double *A = (double*) aligned_alloc(32, 32*32);
    double *B = (double*) aligned_alloc(32, 32*32);
    double *C = (double*) aligned_alloc(32, 32*32);

    memset((void *) A, 0, 32*32);
    memset((void *) B, 0, 32*32);
    memset((void *) C, 0, 32*32); 

    for(int i = 0; i < 32; ++i){
        A[i*32 + i] = 1.0;
        B[i*32 + i] = 1.0;
    }

    dgemm_opt(A,B,C);
    dgemm_opt(A,B,C);
// 
    std::cout << C[0] << std::endl;
    free(A);
    free(B);
    free(C);
}