for (int nb = 0; nb < N; ++nb) {
    c0 = _mm256_setzero_pd();
    c1 = _mm256_setzero_pd();
    c2 = _mm256_setzero_pd();
    c3 = _mm256_setzero_pd();
    c4 = _mm256_setzero_pd();
    c5 = _mm256_setzero_pd();
    c6 = _mm256_setzero_pd();
    c7 = _mm256_setzero_pd();

    for (int kb = 0; kb < K; ++kb) {

      __m256d b = _mm256_broadcast_sd(Bb + kb + nb*K);

      // Load all a vectors
      // Hardware prefetching does heavy lifting
      a0 = _mm256_load_pd(Ab + kb*M);
      a1 = _mm256_load_pd(Ab + 4 + kb*M);
      a2 = _mm256_load_pd(Ab + 8 + kb*M);
      a3 = _mm256_load_pd(Ab + 12 + kb*M);
      a4 = _mm256_load_pd(Ab + 16 + kb*M);
      a5 = _mm256_load_pd(Ab + 20 + kb*M);
      a6 = _mm256_load_pd(Ab + 24 + kb*M);
      a7 = _mm256_load_pd(Ab + 28 + kb*M);

      // Strong hint to register accumulation      
      c0 = _mm256_fmadd_pd(a0, b, c0);
      c1 = _mm256_fmadd_pd(a1, b, c1);
      c2 = _mm256_fmadd_pd(a2, b, c2);
      c3 = _mm256_fmadd_pd(a3, b, c3);
      c4 = _mm256_fmadd_pd(a4, b, c4);
      c5 = _mm256_fmadd_pd(a5, b, c5);
      c6 = _mm256_fmadd_pd(a6, b, c6);
      c7 = _mm256_fmadd_pd(a7, b, c7);


    }
    
    // Store once per 128 iterations of kb
    _mm256_store_pd(Cb + nb*M, c0);
    _mm256_store_pd(Cb + 4 + nb*M, c1);
    _mm256_store_pd(Cb + 8 + nb*M, c2);
    _mm256_store_pd(Cb + 12 + nb*M, c3);
    _mm256_store_pd(Cb + 16 + nb*M, c4);
    _mm256_store_pd(Cb + 20 + nb*M, c5);
    _mm256_store_pd(Cb + 24 + nb*M, c6);
    _mm256_store_pd(Cb + 28 + nb*M, c7);

  }

