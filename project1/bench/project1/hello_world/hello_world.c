#include <unistd.h>
#include <stdio.h>

int main(void){
    char hostname[30];
    if(gethostname(hostname, sizeof(hostname)) == 0){
        printf("%s\n",hostname);
    } else {
        printf("Cannot determine hostname!\n",hostname);
    }
}