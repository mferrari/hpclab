#!/bin/bash
#SBATCH --job-nam=powermethod
echo "n,procs,time,err"

echo -n "8000,1," 
$(which mpirun) -n 1 $(pwd)/powermethod 8000 

echo -n "16000,4," 
$(which mpirun) -n 4 $(pwd)/powermethod 16000 

echo -n "32000,16," 
$(which mpirun) -n 16 $(pwd)/powermethod 32000 

echo -n "64000,64," 
$(which mpirun) -n 64 $(pwd)/powermethod 64000 

echo -n "128000,256," 
$(which mpirun) -npernode 128 -hostfile 2nodes $(pwd)/powermethod 128000 
