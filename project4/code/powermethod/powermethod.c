/****************************************************************
 *                                                              *
 * This file has been written as a sample solution to an        *
 * exercise in a course given at the CSCS-USI Summer School.    *
 * It is made freely available with the understanding that      *
 * every copy of this file must include this header and that    *
 * CSCS/USI take no responsibility for the use of the enclosed  *
 * teaching material.                                           *
 *                                                              *
 * Purpose: : Parallel matrix-vector multiplication and the     *
 *            and power method                                  *
 *                                                              *
 * Contents: C-Source                                           *
 *                                                              *
 ****************************************************************/
#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "hpc-power.h"
#include <float.h>

void print_mat(double* A, const int rows, const int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%f ", A[i*cols + j]); // print the element at (i, j)
        }
        printf("\n"); // move to the next row
    }
}

void rand_vec(double* v, const int N, const int rank) {
    srand(time(NULL) + rank); // seed the random number generator with the current time
    for (int i = 0; i < N; i++) {
        v[i] = ((double) rand()) / RAND_MAX; // generate a random double between 0 and 1
    }
}

double compute_norm(double* v, const int N){
    double acc = 0;
    for(int i = 0; i < N; ++i){
        acc += v[i]*v[i];
    }
    return sqrt(acc);
}

void normalize(double* v, const int N, const double norm) {
    for (int i = 0; i < N; i++) {
        v[i] /= norm; // divide each element of the vector by the norm
    }
}

void mat_vec_mul(double* A, double* x_old, double* x_new, const int N, const int K){
    for(int i = 0; i < K; ++i){
        double acc = 0;
        for(int j = 0; j < N; ++j){
            acc += A[i*N+j] * x_old[j];
        }
        x_new[i] = acc;
    }
}

int main(int argc, char *argv[]) {
    // Check that exactly one command line argument was provided
    if (argc != 2) {
        printf("Usage: %s <number>\n", argv[0]);
        return 1;
    }

    // Convert the argument to an integer
    const int N = atoi(argv[1]);
    const int N_ITER = 1000;
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    // Check
    if(N % size){
        printf("ERROR: Number of rows of A must be divisible by number of procs!\n");
        MPI_Abort(MPI_COMM_WORLD, -1);
    }
    // Generate matrix
    const int K = N/size;
    /*if(rank == 0){
        printf("Allocating %f GB per process.\n", N*K*sizeof(double)/(1e9));
    }*/

    double* A = hpc_generateMatrix(N); // Generate random matrix

    // Allocate vectors
    double * x_old = (double*)calloc(N, sizeof(double));
    double * x_new = (double*)calloc(K, sizeof(double));

    // Generate and distribute initial guess
    rand_vec(x_new, K, rank);
    MPI_Allgather(x_new, K, MPI_DOUBLE, x_old, K, MPI_DOUBLE, MPI_COMM_WORLD);

    /*if(rank == 0){
        printf("Starting solver: N_iter = %i, initial norm |x| = %f\n", N_ITER, compute_norm(x_old, N));
    }*/

    // Start timer & compute eigenvalue
    double start = MPI_Wtime();
    for(int i = 0; i < N_ITER; ++i){
        // Normalize vector
        double norm = compute_norm(x_old, N);
        normalize(x_old, N, norm);

        // Get new partial vector x_new
        mat_vec_mul(A, x_old, x_new, N, K);

        // Update solution
        MPI_Allgather(x_new, K, MPI_DOUBLE, x_old, K, MPI_DOUBLE, MPI_COMM_WORLD);

        /*if(rank == 0){
              printf("ITER %i: norm = %f\n", i, norm);    
        }*/
    }
    double end = MPI_Wtime();
    
    // Get final solution
    double lambda = compute_norm(x_old, N);

    /*if(rank == 0){
        printf("Result: lambda = %f\n", lambda);
    }*/

	double true_lam = hpc_verify(A, N, K);

    if(rank == 0){
        /*
        printf("CHECK: true_lambda = %f\n", true_lam);
        printf("ERROR: ||lambda - true_lambda|| = %f\n", fabs(lambda - true_lam));
        printf("Took %f seconds\n", end-start);
        */
        printf("%f,%f\n", end-start, fabs(lambda - true_lam));
    }
    
    cleanup:
    free(A);
    free(x_old);
    free(x_new);

    MPI_Finalize();
    return 0;
}
