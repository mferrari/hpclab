#!/bin/bash
echo "n,procs,time,err"

echo -n "32768,1," 
$(which mpirun) -n 1 $(pwd)/powermethod 32768

echo -n "32768,2,"
$(which mpirun) -n 2 $(pwd)/powermethod 32768
 
echo -n "32768,4," 
$(which mpirun) -n 4 $(pwd)/powermethod 32768

echo -n "32768,4," 
$(which mpirun) -n 4 $(pwd)/powermethod 32768

echo -n "32768,8," 
$(which mpirun) -n 8 $(pwd)/powermethod 32768

echo -n "32768,16," 
$(which mpirun) -n 16 $(pwd)/powermethod 40000

echo -n "32768,32," 
$(which mpirun) -n 32 $(pwd)/powermethod 32768

echo -n "32768,64," 
$(which mpirun) -n 64 $(pwd)/powermethod 40000

echo -n "32768,128," 
$(which mpirun) -n 128 $(pwd)/powermethod 32768

echo -n "32768,256," 
$(which mpirun) -npernode 128 -hostfile 2nodes $(pwd)/powermethod 32768

echo -n "300000,512,"
$(which mpirun) -npernode 128 -hostfile hosts $(pwd)/powermethod 300032