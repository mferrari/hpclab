echo "alg,n_threads,n,time"
BASE=1000000
export N_POINTS=$BASE
./pi_serial
for i in {1..6}; do
    export N_POINTS=$(($BASE*2**i))
    export OMP_NUM_THREADS=$((2**i));
    ./pi_reduce
    ./pi_critical
done
