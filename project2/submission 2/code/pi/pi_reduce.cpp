#include <functional>
#include <iostream>
#include <chrono>
#include <cmath>
#include "utils.hpp"

#define PI_EXACT 3.14159265359

double midpoint(std::function<double(double)> f, double x0, double h){
    double x = x0 + h*0.5;
    return f(x)*h;
}

double compute_pi(const int N){
    auto phi = [](double x){return 1./(1. + x*x);};

    double h = 1./N;
    double pi = 0;
    
    #pragma omp parallel for reduction(+ : pi)
    for(int i = 0; i < N; ++i){
        pi+=midpoint(phi, i*h, h);
    }
    
    return 4*pi;
}

int main(void){

    // Read environment
    int N = 1000000;
    char* env = std::getenv("N_POINTS");
    if(env){N = str2int(env);}

    int N_THREADS = 1;
    env = std::getenv("OMP_NUM_THREADS");
    if(env){N_THREADS = str2int(env);}

    // Test result
    double pi = compute_pi(N);
    if(std::abs(pi - PI_EXACT) > 1e-6){
        std::cout << "Incorrect result: Pi = " << pi << std::endl;
        return -1;
    }

    double time = 0;
    
    for(int i = 0; i < N_BENCH; ++i){
        #pragma noinline
        {
            auto begin = std::chrono::high_resolution_clock::now();
            compute_pi(N);
            auto end = std::chrono::high_resolution_clock::now();
            time += std::chrono::duration_cast<std::chrono::microseconds>(end-begin).count();
        }
    }
    
    time /= N_BENCH;

    // std::cout << "Pi = " << pi << std::endl;
    // std::cout << "Runtime " << time << " microseconds" << std::endl;
    std::cout << "reduce," << N_THREADS << ","<<N << "," << time << std::endl;

    return 0;
}