echo "size,n_threads,n_iter,time_pp,time_pi,flops"
for i in {1..6}; do
    export OMP_NUM_THREADS=$((2**i));
    echo -n "512,$OMP_NUM_THREADS,"
    ./mandel_omp
done
