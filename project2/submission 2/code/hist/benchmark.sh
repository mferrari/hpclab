echo "n_threads,time"
echo -n "1,"
./hist_seq
for i in {1..6}; do
    export OMP_NUM_THREADS=$((2**i));
    echo -n "$OMP_NUM_THREADS,"
    ./hist_omp
done
