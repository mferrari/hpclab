echo "n_threads,time,sn,norm"
echo -n "1,"
./recur_seq
for i in {1..6}; do
    export OMP_NUM_THREADS=$((2**i));
    echo -n "$OMP_NUM_THREADS,"
    ./recur_omp
done
