import numpy as np
np.set_printoptions(precision=32)
N = 1000
x = np.linspace(0, 1, N, dtype=np.float64)
y = 2**x
A = np.empty((N, 3), dtype=np.float64)
A[:,0] = x**2
A[:,1] = x
A[:,2] = np.ones(N, dtype=np.float64)

ATA = A.T.dot(A)
ATy = A.T.dot(y)

w = np.linalg.solve(ATA, ATy)
print(w)
print(np.log2([1.0000001]))