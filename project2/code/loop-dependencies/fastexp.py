import numpy as np
from time import time

def fastexp(x: float):
    a = 0.3426619246993089
    b = 0.6494447983874846
    c = 1.0037521954197095
    x *= 1.6959938131099002
    f = x % 1
    i = int(x)
    return (1 << i)*(a*x*x + b*x + c)

v = np.random.rand(10000)
start = time()
for i in v:
    3.24**i
print(time() - start)

start = time()
for i in v:
    fastexp(i)
print(time() - start)