#include "walltime.h"
#include <math.h>

int main(int argc, char *argv[]) {
  //int N = 200000000;
  int N = 1 << 28;
  double up = 1.00000001;
  double Sn = 1.00000001;
  int n;
  /* allocate memory for the recursion */
  double *opt = (double *)malloc((N + 1) * sizeof(double));

  if (opt == NULL)
    die("failed to allocate problem size");

  double time_start = wall_time();
  for (n = 0; n <= N; ++n) {
    opt[n] = Sn;
    Sn *= up;
  }
  double time_end = wall_time();
  // printf("Sequential RunTime :  %f seconds\n", wall_time() - time_start);
  // printf("Final Result Sn    :  %.17g \n", Sn);

  double temp = 0.0;
  for (n = 0; n <= N; ++n) {
    temp += opt[n] * opt[n];
  }
  // printf("Result ||opt||^2_2 :  %f\n", temp / (double)N);
  // printf("\n");
  printf("%f,%f,%f\n", time_end-time_start, Sn, temp / (double)N);

  return 0;
}
