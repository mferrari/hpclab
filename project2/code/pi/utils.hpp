int str2int(const char * s){
   char *ptr;
   long ret;

   ret = std::strtol(s, &ptr, 10);
   return (int) ret;
}
