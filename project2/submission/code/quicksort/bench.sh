echo "n_threads,size,time"
BASE=5000000

for j in {6..7}; do
    SIZE=$(($BASE*2**j))
    echo -n "1,"
    ./quicksort $SIZE
done

for i in {6..7}; do
    export OMP_NUM_THREADS=$((2**i));
    for j in {6..7}; do
    SIZE=$(($BASE*2**j))
    echo -n "$OMP_NUM_THREADS,"
    ./quicksort_omp $SIZE
    done
done
