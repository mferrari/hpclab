echo "alg,n_threads,n,time"
export N_POINTS=1000000
./pi_serial
for i in {1..6}; do
    export OMP_NUM_THREADS=$((2**i));
    ./pi_reduce
    ./pi_critical
done
