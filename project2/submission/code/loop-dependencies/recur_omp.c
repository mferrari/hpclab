#include "walltime.h"
#include <math.h>
#include <stdlib.h>
#include <omp.h>
#include <string.h>


int main(int argc, char *argv[]) {
  int N = 1 << 28;
  double up = 1.00000001;
  double Sn = 1.00000001;
  int n;
  /* allocate memory for the recursion */
  double *opt = (double *)malloc(N * sizeof(double));

  if (opt == NULL)
    die("failed to allocate problem size");

  double time_start = wall_time();

  #pragma omp parallel shared(opt)
  {
    int size = omp_get_num_threads();
    int rank = omp_get_thread_num();
    int step = N/size;
    int lo = rank*step;
    int hi;
    if(rank == size-1){hi = N;} else {hi = lo + step;}

    double base = pow(up, lo+1);
    int i;
    for(i = lo; i < hi; ++i){
      opt[i] = base;
      base *= up;
    }
    
  }
  
  Sn = opt[N-1];
  double time_end = wall_time();

  // printf("Parallel RunTime   :  %f seconds\n", wall_time() - time_start);
  // printf("Final Result Sn    :  %.17g \n", Sn);

  double temp = 0.0;
  for (n = 1; n < N+1; ++n) { // Right-shigt
    temp += opt[n] * opt[n];
  }
  // printf("Result ||opt||^2_2 :  %f\n", temp / (double)N);
  // printf("\n");
  printf("%f,%f,%f\n", time_end-time_start, Sn, temp / (double)N);

  return 0;
}
