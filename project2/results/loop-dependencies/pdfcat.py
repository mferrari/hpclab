import sys
import argparse
import pdfrw

def parse_args():
    parser = argparse.ArgumentParser(description="Concatenate PDF files.")
    parser.add_argument("files", nargs="+", help="List of PDF files to concatenate")
    parser.add_argument(
        "-w",
        "--horizontal",
        action="store_true",
        help="Not supported for PDFs. Use a PDF editor to arrange pages horizontally."
    )
    parser.add_argument(
        "-v",
        "--vertical",
        action="store_true",
        help="Concatenate the PDF files vertically",
    )
    parser.add_argument(
        "-p",
        "--padding",
        help="Add padding between images.",
        default=50
    )
    return parser.parse_args()

def concatenate_pdfs(files, horizontal, padding):
    if not horizontal:
        print("Error: Only horizontal concatenation is supported in this version.")
        return

    output_pdf = pdfrw.PdfWriter()
    output_page = None
    current_x_offset = 0

    for file in files:
        input_pdf = pdfrw.PdfReader(file)
        input_page = input_pdf.pages[0]

        if output_page is None:
            output_page = input_page
        else:
            input_page.MediaBox[0] = current_x_offset
            output_page.merge_page(input_page)

        page_width = float(input_page.MediaBox[2]) - float(input_page.MediaBox[0])
        current_x_offset += page_width

    output_pdf.add_page(output_page)
    return output_pdf

def main():
    args = parse_args()

    concatenated_pdf = concatenate_pdfs(args.files, args.horizontal, args.padding)
    if concatenated_pdf is not None:
        concatenated_pdf.write("output.pdf")
        print("PDF files concatenated successfully, saved as output.pdf")
    

main()