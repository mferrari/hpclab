import sys
import argparse
from xml.dom import minidom
import re
import cairosvg


def svg_to_pdf(svg_data, output_filename):
    try:
        cairosvg.svg2pdf(svg_data, write_to=output_filename)
        print(f"SVG successfully converted to PDF, saved as {output_filename}")
    except Exception as e:
        print(f"Error converting SVG to PDF: {e}")

def parse_args():
    parser = argparse.ArgumentParser(description="Concatenate SVG files.")
    parser.add_argument("files", nargs="+", help="List of SVG files to concatenate")
    parser.add_argument(
        "-w",
        "--horizontal",
        action="store_true",
        help="Concatenate the images horizontally",
    )
    parser.add_argument(
        "-v",
        "--vertical",
        action="store_true",
        help="Concatenate the images vertically",
    )
    parser.add_argument(
        "-p",
        "--padding",
        help="Set padding between images.",
        default=50
    )

    parser.add_argument(
        "-pdf",
        "--convert-to-pdf",
        action="store_true",
        help="Convert output to pdf",
        default=True
    )
    return parser.parse_args()


def parse_length(value):
    match = re.match(r"(\d+(?:\.\d+)?)(\D*)", value)
    if not match:
        raise ValueError(f"Invalid length value: {value}")
    return float(match.group(1)), match.group(2)

def convert_to_pixels(value, unit):
    conversion_factors = {
        "px": 1,
        "pt": 1.25,
        "pc": 15,
        "mm": 3.543307,
        "cm": 35.43307,
        "in": 90,
    }

    if unit not in conversion_factors:
        raise ValueError(f"Unsupported unit: {unit}")

    return value * conversion_factors[unit]

def concatenate_svgs(files, horizontal, vertical, pad):
    if not horizontal and not vertical:
        raise ValueError("Either horizontal or vertical concatenation must be chosen.")

    output_doc = minidom.Document()
    output_svg = output_doc.createElement("svg")
    output_doc.appendChild(output_svg)

    max_width = 0
    max_height = 0
    current_x = 0
    current_y = 0

    for file in files:
        with open(file, "r") as f:
            content = f.read()
            input_doc = minidom.parseString(content)
            input_svg = input_doc.documentElement
            width_value, width_unit = parse_length(input_svg.getAttribute("width"))
            height_value, height_unit = parse_length(input_svg.getAttribute("height"))

            width = convert_to_pixels(width_value, width_unit)
            height = convert_to_pixels(height_value, height_unit)

            if horizontal:
                input_svg.setAttribute("x", str(current_x))
                current_x += width + pad
                max_width += width + pad
                max_height = max(max_height, height)
            else:
                input_svg.setAttribute("y", str(current_y))
                current_y += height + pad
                max_height += height + pad
                max_width = max(max_width, width)

            output_svg.appendChild(input_svg)

    output_svg.setAttribute("width", str(max_width))
    output_svg.setAttribute("height", str(max_height))
    output_svg.setAttribute("xmlns", "http://www.w3.org/2000/svg")

    return output_doc.toxml()

            
def main():
    args = parse_args()
    try:
        concatenated_svg = concatenate_svgs(args.files, args.horizontal, args.vertical, float(args.padding))
        with open("output.svg", "w") as output_file:
            if args.convert_to_pdf:
                svg_to_pdf(concatenated_svg, "output.pdf")
            else:
                output_file.write(concatenated_svg)
                print("SVG files concatenated successfully, saved as output.svg")
    except Exception as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    main()