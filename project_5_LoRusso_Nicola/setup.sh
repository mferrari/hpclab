#!/bin/bash

module load gcc/6.3.0 julia eth_proxy
export JULIA_DEPOT_PATH=”$HOME/.julia:/cluster/project/sam/karoger-shared/julia:$JULIA_DEPOT_PATH”
srun --time=4:00:00 --mem-per-cpu=8G --x11 --pty bash

/cluster/home/nlorusso/project5-code