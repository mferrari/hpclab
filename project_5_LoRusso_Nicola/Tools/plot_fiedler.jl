#   M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
"""
    draw_fiedler(A, coords)

Plot the entries of the Fiedler vector of graph `A`.
"""
function draw_fiedler(A, coords, name)
    
    #   1.  Get x and y coordinates
    x = coords[:,1]
    y = coords[:,2]

    #   2.  Compute fiedler vector entries
    D = sum(A, dims=2)
    L = spdiagm(vec(D)) - A

    lambda, U = eigs(L; nev=47, which=:SR, tol=1e-4, maxiter=500)
    eig1 = U[:, 1]
    eig1 = eig1/norm(eig1)
    eig2 = U[:, 2]
    eig2 = eig2/norm(eig2)

    #   3.  Compute the indicator vector
    indicator1 = sign.(eig1)
    indicator2 = sign.(eig2)

    #   4.  Plot using e.g. Makie.scatter
    eigplot = plot(1:length(eig1), eig1, label="eig1", markersize = 5, color=:blue)
    plot!(1:length(eig2), eig2, label="eig2", markersize = 5, color=:red)
    save("./Figures/plot_fiedler/$(name)-eigenvectors-fielder.png", eigplot)

    fig= Figure()
    ax = Axis3(fig[1, 1])

    scatter!(ax, x, y, eig2, color = indicator2, colormap = [:blue, :red], markersize = 5)
  
    save("./Figures/plot_fiedler/$(name)-fielder.png", fig)


    #   Last subtask
    eig3 = U[:, 3]
    eig3 = eig3/norm(eig3)

    indicator3 = sign.(eig3)
    
    new_coords = hcat(eig2,eig3)

    p = spectral_part(A)

    fig2 = draw_graph(A, new_coords, p)
    save("./Figures/plot_fiedler/$(name)-spectral-coords.png", fig2)

end