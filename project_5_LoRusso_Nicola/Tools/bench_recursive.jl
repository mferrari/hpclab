#   M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
"""
    benchmark_recursive()

Run a benchmark of different meshes with different recursive partitioning method.

# Examples
```julia-repl
julia> function benchmark_recursive()
()
```
"""
function benchmark_recursive()

    #   List the meshes to compare
    #meshes = ["mesh3e1" "airfoil1" "netz4504_dual" "stufe" "3elt" "barth4" "ukerbe1" "crack"]
    meshes = ["mesh3e1" "airfoil1" "3elt" "barth4" "crack"]


    #   List the algorithms to recursively run and compare
    algs = ["Spectral" "Spectral" "Metis" "Metis" "Coordinate" "Coordinate" "Inertial" "Inertial"]

    #   Init result array
    pAll = Array{Any}(undef, length(meshes), length(algs) + 1)

    #   Loop through meshes
    for (i, mesh) in enumerate(meshes)
        #print("Mesh", mesh, "\n")
        #   Define path to mat file
        path = joinpath(dirname(@__DIR__),"Meshes","2D",mesh*".mat");

        #   Read data
        A, coords = read_mat_graph(path);

        #   1st row
        pAll[i, 1] = mesh

        #   Recursive routines
        #   1.  Spectral
        pSpectral1 = rec_bisection("spectral_part", 3, A)
        pSpectral2 = rec_bisection("spectral_part", 4, A)

        # if mesh == "crack"
        #     fig = draw_graph(A, coords, pSpectral2)
        #     save("./Figures/bench_recursive/spectral.png",fig)
        # end

        pAll[i, 2] = count_edge_cut(A, pSpectral1)
        pAll[i, 3] = count_edge_cut(A, pSpectral2)

        # #   2.  METIS
        pMETIS1 = metis_part(A, 8, :RECURSIVE)
        pMETIS2 = metis_part(A, 16, :RECURSIVE)

        # if mesh == "crack"
        #     fig = draw_graph(A, coords, pMETIS2)
        #     save("./Figures/bench_recursive/metis.png",fig)
        # end
        pAll[i, 4] = count_edge_cut(A, pMETIS1)
        pAll[i, 5] = count_edge_cut(A, pMETIS2)

        
        #   3.  Coordinate

        pCoordinate1 = rec_bisection("coordinate_part", 3, A, coords)
        pCoordinate2 = rec_bisection("coordinate_part", 4, A, coords)

        # if mesh == "crack"
        #     fig = draw_graph(A, coords, pCoordinate2)
        #     save("./Figures/bench_recursive/coordinate.png",fig)
        # end

        pAll[i, 6] = count_edge_cut(A, pCoordinate1)
        pAll[i, 7] = count_edge_cut(A, pCoordinate2)
        
        #   4.  Inertial

        pInertial1 = rec_bisection("inertial_part", 3, A, coords)
        pInertial2 = rec_bisection("inertial_part", 4, A, coords)

        # if mesh == "crack"
        #     fig = draw_graph(A, coords, pInertial2)
        #     save("./Figures/bench_recursive/inertial.png",fig)
        # end

        pAll[i, 8] = count_edge_cut(A, pInertial1)
        pAll[i, 9] = count_edge_cut(A, pInertial2)

    end

    #   Print result table
    header =(hcat(["Mesh"], algs), ["" "8 parts" "16 parts" "8 parts" "16 parts" "8 parts" "16 parts" "8 parts" "16 parts"])
    pretty_table(pAll; header = header, crop = :none, header_crayon = crayon"bold cyan")
end

