#   M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
#   Data extraction method
"""
    read_csv_graph(path_file)

Extract the data located in the csv `path_file`.
"""

function read_csv_graph(path_file)
    
    #   1.  Load the .csv files for edges and coords
    edges = readdlm(path_file, ',', Int, skipstart=1)    
    coords_file = replace(path_file, "-adj.csv" => "-pts.csv")
    coords = readdlm(coords_file, ',', skipstart=1)

    #   2.  Construct the adjacency matrix A (sparse)
    n = maximum(edges)
    A = spzeros(n, n)

    for row in eachrow(edges)
        i = row[1]
        j = row[2]                
        A[i,j] = 1
        A[j,i] = 1
    end
  
    # Check if the A is symmetric
    if !LinearAlgebra.issymmetric(A)
        A = (A + transpose(A)) / 2
    end

    #   3.  Visualize and save the result 

    # Extract name and create folder
    name = splitext(basename(path_file))[1]
    country_name = name[1:2]

    mat_folder = "./Meshes/Countries/mat"
    png_folder = "./Meshes/Countries/png"

    # Save figure as png 
    fig = draw_graph(A, coords)
    save(joinpath(png_folder, "$(country_name).png"),fig)

    # Write the matrices to a Matlab file
    matwrite(joinpath(mat_folder, "$(country_name)_adj.mat"), Dict("A" => A))
    matwrite(joinpath(mat_folder, "$(country_name)_pts.mat"), Dict("coords" => coords))

    #   4.  Return the matrix A and the coordinates 
    return A, coords
end