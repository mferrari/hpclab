#   M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
"""
    benchmark_metis()

Run a benchmark of different meshes with METIS partitioning method.

# Examples
```julia-repl
julia> benchmark_metis()
```
"""
function benchmark_metis()

     # Load matrices and coordinates
     matrices = [       ("./Meshes/Roads/luxembourg_osm.mat", "Luxemburg"),         
                        ("./Meshes/Roads/usroads.mat", "usroads-48"),         
                        ("./Meshes/Countries/csv/GR-3117-adj.csv", "Greece"),        
                        ("./Meshes/Countries/csv/CH-4468-adj.csv", "Switzerland"),         
                        ("./Meshes/Countries/csv/VN-4031-adj.csv", "Vietnam"),         
                        ("./Meshes/Countries/csv/NO-9935-adj.csv", "Norway"),        
                        ("./Meshes/Countries/csv/RU-40527-adj.csv", "Russia")    ]

     pAll = Array{Any}(undef, 2, length(matrices) * 2 + 1)
     pAll[1, 1] = 16
     pAll[2, 1] = 32


     regions = ["Luxemburg" "Luxemburg" "usroads-48" "usroads-48" "Greece" "Greece" "Switzerland" "Switzerland" "Vietnam" "Vietnam" "Norway" "Norway" "Russia" "Russia"]

     png_folder_METIS = "./Figures/bench_metis"
 
     # Partition matrices and count edge cuts
     for (i, (filename, region)) in enumerate(matrices)
         A, coords = splitext(filename)[2] == ".mat" ? read_mat_graph(filename) : read_csv_graph(filename)
         p_Rec16 = metis_part(A, 16, :RECURSIVE)
         p_Rec32 = metis_part(A, 32, :RECURSIVE)
         p_KWAY16 = metis_part(A, 16, :KWAY)
         p_KWAY32 = metis_part(A, 32, :KWAY)
 
         pAll[1, i*2] = count_edge_cut(A, p_Rec16)
         pAll[2, i*2] = count_edge_cut(A, p_Rec32)
         pAll[1, i*2+1] = count_edge_cut(A, p_KWAY16)
         pAll[2, i*2+1] = count_edge_cut(A, p_KWAY32)

         if region=="usroads-48" || region=="Luxemburg" || region=="Russia"
            fig_rec32 = draw_graph(A, coords, p_Rec32)
            fig_KWAY32 = draw_graph(A, coords, p_KWAY32)

            save(joinpath(png_folder_METIS, "$(region)_rec32.png"),fig_rec32)
            save(joinpath(png_folder_METIS, "$(region)_KWAY32.png"),fig_KWAY32)

         end
 
     end


    #   3.  Visualize the results for 32 partitions.
    header =(hcat(["Partitions"], regions), ["" "Recursive" "kway" "Recursive" "kway" "Recursive" "kway" "Recursive" "kway" "Recursive" "kway" "Recursive" "kway" "Recursive" "kway"])
    pretty_table(pAll; header = header, crop = :none, header_crayon = crayon"bold red")

end