#!/bin/bash
#SBATCH --job-name=PDE
#SBATCH --output=strong.csv
#SBATCH --cpus-per-task=64

echo "threads,n,time"
for j in {6..10}; do
    SIZE=$((2**j));
    for i in {0..7}; do
        export OMP_NUM_THREADS=$((2**i));
        echo -n $OMP_NUM_THREADS,$SIZE,
        ./main $SIZE 100 0.005
    done
done