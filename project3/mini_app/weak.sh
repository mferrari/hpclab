#!/bin/bash
#SBATCH --job-name=PDE
#SBATCH --output=weak.csv
#SBATCH --cpus-per-task=64

echo "threads,n,time"
for i in {0..3}; do
    export OMP_NUM_THREADS=$((4**i));
    SIZE=$((2**(i+7)));
    echo -n $OMP_NUM_THREADS,$SIZE,
    ./main $SIZE 100 0.005
done
