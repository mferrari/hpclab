#   M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
#   Main file of Project 5

#   I/O packages
using DelimitedFiles, MAT
#   Math packages
using Arpack, LinearAlgebra, Metis, Random, SparseArrays, Statistics
#   Plot packages
using Graphs, SGtSNEpi, Colors, CairoMakie, PrettyTables

#   Tools
include("./Tools/add_paths.jl");

#   Generate adjacency matrices and vertices coordinates
path_file = "./Meshes/2D/airfoil1.mat" # or "/path/to/project/Sources/Meshes/Meshes/2D/airfoil1.mat
A, coords = read_mat_graph(path_file);
#   Draw & save the airfoil1
fig = draw_graph(A, coords)
save("airfoil1.pdf", fig)


#   START: Exercise 1________________________________________________________
path_NO = "./Meshes/Countries/csv/NO-9935-adj.csv"
path_VN = "./Meshes/Countries/csv/VN-4031-adj.csv"

A_NO, coords_NO = read_csv_graph(path_NO);
A_VN, coords_VN = read_csv_graph(path_VN);

#   END______________________________________________________________________


#   Run benchmark
#   START: Exercise 2________________________________________________________
benchmark_bisection()
#   END______________________________________________________________________

#   START: Exercise 3________________________________________________________
benchmark_recursive()
#   END______________________________________________________________________


#   START: Exercise 4________________________________________________________
benchmark_metis()
#   END______________________________________________________________________



#   START: Exercise 5________________________________________________________
#  Generate adjacency matrices and vertices coordinates

matrices = [        ("./Meshes/2D/airfoil1.mat", "airfoil1"),         
                    ("./Meshes/2D/mesh3e1.mat", "mesh3e1"),         
                    ("./Meshes/2D/barth4.mat", "barth4"),        
                    ("./Meshes/2D/3elt.mat", "3elt"),         
                    ("./Meshes/2D/crack.mat", "crack")      
             ]

for (i, (filename, name)) in enumerate(matrices)
    A_mat, coords_mat = read_mat_graph(filename)

    p_mat = spectral_part(A_mat)

    fig_mat = draw_graph(A_mat, coords_mat, p_mat)
    save("./Figures/plot_fiedler/$(name)-spatial-coords.png",fig_mat)

    draw_fiedler(A_mat, coords_mat, name)
end
#   END______________________________________________________________________


      


