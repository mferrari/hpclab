#   M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
"""
    inertial_part(A, coords)

Compute the bi-partions of graph `A` using inertial method based on the `coords` of the graph.

# Examples
```julia-repl
julia> inertial_part(A, coords)
 1
 ⋮
 2
```
"""
function inertial_part(A, coords)
    
    #   1.  Compute the center of mass.
    n = size(coords,1)
    center_of_mass = sum(coords, dims=1) / n


    x = coords[:,1]
    y = coords[:,2]
    x_mean = center_of_mass[1]
    y_mean = center_of_mass[2]


    #   2.  Construct the matrix M. (see pdf of the assignment)
    xx = sum((x .- x_mean).^2);
    xy2 = sum((x .- x_mean).*(y .- y_mean));
    yy = sum((y .- y_mean).^2)
    M = [yy xy2; xy2 xx]
    

    #   3.  Compute the eigenvector associated with the smallest eigenvalue of M.
    lambda, V = eigs(M; nev=1, which=:SR, tol=1e-4, maxiter=500)
    U = V[:, 1]
    U = U/norm(U)

    #   4.  Partition the nodes around line L 
    #       (use may use the function partition(coords, eigv))
    part1, part2 = partition(coords, U)

    #   5.  Return the indicator vector
    indicator = zeros(Int64, n)
    indicator[part1].=1
    indicator[part2].=2

    # RANDOM PARTITIONING - REMOVE AFTER COMPLETION OF THE EXERCISE
    # n = size(A)[1];
    # rng = MersenneTwister(1234);
    # p = bitrand(rng, n);
    return indicator

end